# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

# Set graphical systemd target as default

%post

ln -sf /lib/systemd/system/graphical.target /etc/systemd/system/default.target

%end
