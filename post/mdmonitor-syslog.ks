# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

# Override the mdmonitor service to log to the syslog

%post

mkdir /etc/systemd/system/mdmonitor.service.d
cat > /etc/systemd/system/mdmonitor.service.d/local.conf <<'EOF'
[Service]
ExecStart=
ExecStart=/sbin/mdadm --monitor --scan --syslog -f --pid-file=/var/run/mdadm/mdadm.pid
EOF

%end
