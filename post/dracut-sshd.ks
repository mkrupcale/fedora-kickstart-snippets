# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

# Install dracut-sshd module[1] to enable SSH access during initramfs stage of
# boot. Can be useful to enter passphrase to decrypt LUKS on headless
# systems. If we do this, we need to re-build initramfs using dracut(8)
# following dracut module and kernel installation
# [1] https://github.com/gsauthof/dracut-sshd

%post

# create host SSH keys
for key_type in ecdsa ed25519 rsa; do
/usr/libexec/openssh/sshd-keygen ${key_type}
done

# create sshd dracut module
DRACUT_MODULE_DIR=/usr/lib/dracut/modules.d
mkdir ${DRACUT_MODULE_DIR}/46sshd

GIT_COMMIT=4fd59df9d1145d5910d45a91bd64f3d7079a0fc9
for f in module-setup.sh sshd.service sshd_config; do
wget -O ${DRACUT_MODULE_DIR}/46sshd/${f} https://github.com/gsauthof/dracut-sshd/raw/${GIT_COMMIT}/46sshd/${f}
done

# ensure dracut networkd service will be started
cat > /etc/systemd/network/20-wired.network <<'EOF'
[Match]
Name=en*

[Network]
DHCP=yes
EOF

# rebuild initramfs
KERNEL_VERSION=$(rpm -q kernel --qf '%{version}-%{release}.%{arch}\n')
dracut -f /boot/initramfs-${KERNEL_VERSION}.img ${KERNEL_VERSION}

%end
