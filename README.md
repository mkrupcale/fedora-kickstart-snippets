# fedora-kickstart-snippets

## Introduction

This repository contains kickstart snippets for inclusion in complete kickstart files used for automating the Fedora installation. Kickstart installation requires the `netinstall` media type, or a direct installation booting method such as PXE; kickstarts are not supported with live images[1].

## Usage

These files are just snippets and therefore should be `%include`ed into a complete kickstart file. This can be a path to the file for disk-based installation or a URL for network installations. See the kickstart syntax reference[2] for more information.

The complete kickstart file is specified at the boot menu once bootloader is loaded[3,4]. The easiest way to specify the kickstart file is probably to host it on an HTTP(S) server and use `inst.ks=http[s]://<host>/<path>`.

## References

1. [Fedora Installation Guide: Advanced Installation Options - Automating the Installation with Kickstart](https://docs.fedoraproject.org/en-US/fedora/f29/install-guide/advanced/Kickstart_Installations/#chap-kickstart-installations)
2. [Kickstart Syntax Reference](https://docs.fedoraproject.org/en-US/fedora/f29/install-guide/appendixes/Kickstart_Syntax_Reference/)
3. [Fedora Installation Guide: Advanced Installation Options - Boot Options](https://docs.fedoraproject.org/en-US/fedora/f29/install-guide/advanced/Boot_Options/#sect-boot-options-kickstart)
4. [Anaconda Documentation: Anaconda Boot Options](https://anaconda-installer.readthedocs.io/en/latest/boot-options.html#inst-ks)
