# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

%addon com_redhat_kdump --disable --reserve-mb='128'
%end
