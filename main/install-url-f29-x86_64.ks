# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

# Use network installation
install
url --url="http://mirrors.mit.edu/fedora/linux/releases/29/Everything/x86_64/os/"
